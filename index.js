'use strict';

const math = require('mathjs');

// Sigmoid function as activation function
const activation = m => math.map(m, x => 1 / (1 + math.exp(-x)));
// The derivative of the Sigmoid function
const gradient = m => math.map(m, x => x * (1 - x));

class NeuralNetwork {
    constructor(dim) {
        this.weights = math.random(dim);
    }
    think(inputs) {
        return activation(math.multiply(inputs, this.weights));
    }
    train(training) {
        for (let i = 0; i < training.iterations; i++) {
            const output = this.think(training.inputs);
            const error = math.subtract(training.outputs, output);
            const adjustment = math.multiply(
                math.transpose(training.inputs),
                math.dotMultiply(error, gradient(output))
            );
            this.weights = math.add(this.weights, adjustment);
        }
    }
}

module.exports = NeuralNetwork;
