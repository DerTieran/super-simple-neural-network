# Super Simple Neural Network

This is a super simple neural network, with only the [mathjs](https://github.com/josdejong/mathjs) library as it's dependency.

## Install

To run this neural network you need [`nodejs`](https://nodejs.org/en/) and then run

```sh
npm install
```

Then the examples can be run by

```sh
node or.js
```

and

```sh
node other.js
```

Alternatily this could also be run in the browser by including `mathjs`.

## How does it work

First you pass in the dimention of the neural network to generate some random starting weights.
Then we start trainign the neural network with the training data.

It takes the input and adjusts them by the weights and pass them to the activation function to calculate the neurons output.
To do this the neural network first multiply the input with it's weight and sums them up. To then normalise the result the Sigmoid function is used wich give us a value between zero and one.

Now the error is calculate from the difference of the training output and the neurons output.
Now depending on the gradient, meaning the direction and steepnes, of the error we adjust the weights.
To calculate the gradient we use the derivative of the Sigmoid function, simplified by passing in the output.
The adjustment is then calculated by multipling the input the error and the gradient.
By using the derivative of the Sigmoid function we get small adjustments if we are _confident_ and larger ones if we are less _confident_.
Then the new weights are calculate by adding the adjustment to the old weights.

This process is reapeated multiple times til the weights of the neural network reach an optimum and we can pass in a new situation.

## OR Example

To test the neural network we use a simple OR gate.

| a   | b   | a OR b |
| --- | --- | ------ |
| 0   | 0   | 0      |
| 0   | 1   | 1      |
| 1   | 0   | 1      |
| 1   | 1   | 1      |

We take the last row as our test set and the rest as our training set.
The dimentionality of our set is `2 x 1`, with two inputs and one output.
We pass in the training set and adjust the weights `10000` times and get the following output:

```sh
Starting weights: [ [ 0.8863448463690945 ], [ 0.12944636781726754 ] ]
Weights after training: [ [ 4.9305952186994615 ], [ 4.930157864277455 ] ]
[1, 1] => [ 0.9999478196862093 ]
```

So for the new situation `[1, 1]` we get the prediction of 0.99995 wich is nearly 1 wich was the correct result.

## Other example

Now we try a bit more complex example, in this the output is one if b is one otherwise it is zero.

| a   | b   | c   | output |
| --- | --- | --- | ------ |
| 0   | 0   | 0   | 0      |
| 0   | 0   | 1   | 0      |
| 0   | 1   | 1   | 1      |
| 1   | 1   | 1   | 1      |
| 1   | 1   | 0   | 1      |
| 1   | 0   | 1   | 0      |

Here we take the last two rows as our test data and the rest as training data.
In this problem we have a dimentionality of `3 x 1`, with three inputs and one output.
We again iterate `10000` times and get the following output:

```sh
Starting weights:
[ [ 0.26719792482639043 ],
  [ 0.13755036868055187 ],
  [ 0.512801028248717 ] ]
Weights after training:
[ [ 1.502917028987414 ],
  [ 8.966510303235893 ],
  [ -4.367174829196995 ] ]
[1, 1, 0] => [ 0.9999716094928267 ]
[1, 0, 1] => [ 0.053948975641523525 ]
```

So again the neural network predicted the output pretty closely to the actuall result.

To get even more precise results we could increase the iterations or adjust the algorythms, adding a minimum firing threshold or using a different formular.
So again the neural network predicted the output pretty closely to the actuall result.

To get even more precise results we could increase the iterations or adjust the algorythms, adding a minimum firing threshold or using a different formular.
