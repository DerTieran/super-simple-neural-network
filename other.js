'use strict';

const NeuralNetwork = require('.');

const nn = new NeuralNetwork([3, 1]);

console.log('Starting weights:');
console.log(nn.weights);

// The other training set.
// | 0 | 0 | 0 | 0 |
// | 0 | 0 | 1 | 0 |
// | 0 | 1 | 1 | 1 |
// | 1 | 1 | 1 | 1 |
const inputs = [[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 1, 1]];
const outputs = [[0], [0], [1], [1]];
const iterations = 100000;
nn.train({ inputs, outputs, iterations });

console.log('Weights after training:');
console.log(nn.weights);

// Test with new situations.
// | 1 | 1 | 0 | 1 |
console.log('[1, 1, 0] =>', nn.think([1, 1, 0]));

// | 1 | 0 | 1 | 0 |
console.log('[1, 0, 1] =>', nn.think([1, 0, 1]));
