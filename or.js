'use strict';

const NeuralNetwork = require('.');

const nn = new NeuralNetwork([2, 1]);

console.log('Starting weights:', nn.weights);

// The OR training set.
// | 0 | 0 | 0 |
// | 0 | 1 | 1 |
// | 1 | 0 | 1 |
const inputs = [[0, 0], [0, 1], [1, 0]];
const outputs = [[0], [1], [1]];
const iterations = 10000;
nn.train({ inputs, outputs, iterations });

console.log('Weights after training:', nn.weights);

// Test with new situation.
// | 1 | 1 | 1 |
console.log('[1, 1] =>', nn.think([1, 1]));
